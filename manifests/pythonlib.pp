# Deploy the Tails pythonlib on systems from git.
#
# The `$ensure' parameter is passed through to Vcsrepo resources,
# and (with some translation) to File and ensure_packages resources.
#
# Consumers or this class need to include it and set:
#
#     PYTHONPATH='${tails::pythonlib::pythonpath}'
#
class tails::pythonlib (
  String $ensure                      = 'latest',
  Stdlib::Absolutepath $clone_basedir = '/var/lib/tails_pythonlib',
  String $repo_origin                 = 'https://git.tails.boum.org/tails',
  String $repo_rev                    = 'stable',
  String $repo_user                   = 'root',
) {

  $wc_dir = "${clone_basedir}/tails"
  $pythonpath = "${wc_dir}/config/chroot_local-includes/usr/lib/python3/dist-packages"

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  file { $clone_basedir:
    ensure => $directory_ensure,
    owner  => $repo_user,
    group  => $repo_user,
    mode   => '0755',
  }

  $packages = [
    'python3-sh',
  ]

  $package_ensure = $ensure ? {
    absent  => absent,
    default => present,
  }

  ensure_packages(
    $packages,
    {'ensure' => $package_ensure}
  )

  vcsrepo { $wc_dir:
    ensure   => $ensure,
    provider => git,
    source   => $repo_origin,
    revision => $repo_rev,
    user     => $repo_user,
    require  => File[$clone_basedir],
  }

}
