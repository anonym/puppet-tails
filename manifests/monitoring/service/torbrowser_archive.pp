# Manages the monitoring of the Tails' Tor Browser archive.
# Use $name to specify the hostname of the server hosting the archives,
# $real_host to specify on which host this service is running.

define tails::monitoring::service::torbrowser_archive (
  String $real_host,
  Enum['present', 'absent'] $ensure = present,
  String $nodename                  = 'ecours.tails.boum.org',
  Boolean $enable_notifications     = true,
  String $check_interval            = '10m',
  String $retry_interval            = '2m',
  Optional[String] $display_name    = undef,
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::torbrowser_archive

  file { "/etc/icinga2/conf.d/${name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/torbrowser_archive.erb'),
    require => Class['::tails::monitoring::checkcommand::torbrowser_archive'],
    notify  => Service['icinga2'],
  }

}
