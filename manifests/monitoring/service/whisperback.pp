# Manages the monitoring of a smtp server behind a hidden service,
# typically used in Tails to support the Whiperback bug reporting tool.
# $name must be the hidden service onion name, $real_host must point to
# the host where this service is running.

define tails::monitoring::service::whisperback (
  String $real_host,
  Enum['present', 'absent'] $ensure = present,
  String $nodename                  = 'ecours.tails.boum.org',
  Boolean $enable_notifications     = true,
  String $flapping_threshold        = '30',
  String $check_interval            = '10m',
  String $retry_interval            = '2m',
  Integer $max_check_attempts       = 8,
  Optional[String] $display_name    = undef,
){

  validate_re($name, '^[a-z0-9]+\.onion$')

  if $display_name == undef {
    $sp_name = split($name, '\.')
    $service_name = $sp_name[0]
    $displayed_name = "Whisperback_${service_name}"
  } else {
    $displayed_name = $display_name
  }

  file { "/etc/icinga2/conf.d/whisperback_${service_name}.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/whisperback.erb'),
    require => Class['::tails::monitoring::checkcommand::torified_smtp'],
    notify  => Service['icinga2'],
  }

}
