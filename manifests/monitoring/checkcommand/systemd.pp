class tails::monitoring::checkcommand::systemd (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_systemd

  file { '/etc/icinga2/conf.d/check_systemd.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_systemd.conf',
    require => Class['tails::monitoring::plugin::check_systemd'],
    notify  => Service['icinga2'],
  }

}
