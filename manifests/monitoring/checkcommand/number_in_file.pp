class tails::monitoring::checkcommand::number_in_file (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_number_in_file

  file { '/etc/icinga2/conf.d/check_number_in_file.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_number_in_file.conf',
    require => Class['tails::monitoring::plugin::check_number_in_file'],
    notify  => Service['icinga2'],
  }

}
