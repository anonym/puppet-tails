class tails::monitoring::plugin::check_systemd (
  Enum['present', 'absent'] $ensure  = 'present',
){

  file {'/usr/lib/nagios/plugins/pynagsystemd':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/monitoring/icinga2/plugins/pynagsystemd',
    notify  => Service['icinga2'],
    require => Package['python-nagiosplugin'],
  }

  ensure_packages(['python-nagiosplugin'])

}
