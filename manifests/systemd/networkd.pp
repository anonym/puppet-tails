# Manage systemd-networkd
#
# For now, this class assumes that something else configures network
# interfaces via /etc/systemd/network/*.network
class tails::systemd::networkd () {
  # Don't let ifupdown manage en* nor eth*
  file_line { 'ifupdown-disable-allow-hotplug':
    path  => '/etc/network/interfaces',
    match => 'allow-hotplug',
    line  => '# allow-hotplug: disabled by Puppet',
  }
  file_line { 'ifupdown-disable-iface-en':
    path  => '/etc/network/interfaces',
    match => 'iface en',
    line  => '# iface: disabled by Puppet',
  }
  file_line { 'ifupdown-disable-iface-eth':
    path  => '/etc/network/interfaces',
    match => 'iface eth',
    line  => '# iface: disabled by Puppet',
  }

  service { 'systemd-networkd':
    ensure => 'running',
    enable => true,
  }
  service { 'systemd-networkd-wait-online.service':
    enable => true,
  }
}
