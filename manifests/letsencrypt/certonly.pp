# Manages Let's Encrypt certificates for a set of domains
#
# This is a thin wrapper around letsencrypt::certonly, that sets some of its
# parameters to sensible values for our use cases.
#
# We don't bother checking the arguments' validity since we're merely passing
# them through to letsencrypt::certonly, that will do it itself.
#
define tails::letsencrypt::certonly (
  Array[Stdlib::Fqdn] $domains               = [$title],
  String $plugin                             = 'webroot',
  Array[Stdlib::Absolutepath] $webroot_paths = ['/var/www/html'],
  Array[String] $additional_args             = [],
) {

  ::letsencrypt::certonly { $title:
    plugin          => $plugin,
    domains         => $domains,
    webroot_paths   => $webroot_paths,
    additional_args => $additional_args,
    manage_cron     => false,
  }

}
