# Manage a cronjob that updates time-based snapshots of APT repositories
# in a reprepro setup.
#
# This defined resource is meant to be declared up to 4 times on a given system.
# Its $id parameter must be an integer among [1..4].
#
# The serial number of the generated snapshots uses the configured system
# timezone, which should be set to UTC for better consistency with other date
# information embedded e.g. in the mirrored APT repositories.
define tails::reprepro::snapshots::time_based::cronjob (
  Pattern[/\A[a-zA-Z-]+\z/] $repository,
  Stdlib::Absolutepath $basedir,
  Integer[1, 4] $id,
  Integer[0, 23] $delta_hours       = 0,
  Integer[0, 59] $delta_mins        = 0,
  Enum['present', 'absent'] $ensure = 'present',
) {

  $id_str = String($id)

  cron { "tails-update-time-based-apt-snapshots-${repository}-${id_str}":
    ensure  => $ensure,
    command => "SILENT=1 PATH=\"/usr/local/bin:\$PATH\" flock -n '/var/lock/tails-delete-expired-apt-snapshots-${repository}' tails-delete-expired-apt-snapshots '${basedir}' && SILENT=1 PATH=\"/usr/local/bin:\$PATH\" flock -n '/var/lock/tails-update-time-based-apt-snapshots-${repository}' tails-update-time-based-apt-snapshots '${repository}' '${basedir}' \"$(date '+\\%Y\\%m\\%d')0${id_str}\"", # lint:ignore:140chars -- command
    user    => 'reprepro-time-based-snapshots',
    hour    => ($delta_hours + 6 * ($id - 1)) % 24,
    minute  => $delta_mins % 60,
    require => [
      File['/usr/local/bin/tails-update-time-based-apt-snapshots'],
      File['/usr/local/bin/tails-delete-expired-apt-snapshots'],
      Reprepro::Repository["tails-time-based-snapshots-${repository}"],
    ],
  }

  cron { "tails-stats-time-based-apt-snapshots-${repository}-${id_str}":
    ensure  => $ensure,
    command => "stat --format='\\%s' '${basedir}/db/packages.db' > '${basedir}/project/packages.db.size' && stat --format='\\%s' '${basedir}/db/references.db' > '${basedir}/project/references.db.size'", # lint:ignore:140chars -- command
    user    => 'reprepro-time-based-snapshots',
    hour    => ($delta_hours + 6 * ($id - 1)) % 24,
    minute  => ($delta_mins - 10) % 60,
    require => [
      File["${basedir}/project/packages.db.size"],
      File["${basedir}/project/references.db.size"],
    ],
  }

}
