# Manage various email reminders

class tails::meeting::reminders {

  include tails::meeting

  tails::meeting::reminder { 'FT_meeting':
    addresses    => ['tails-foundations@boum.org'],
    subject      => 'Tails Foundations Team meeting: ',
    template     => 'FT_meeting_template.eml',
    day_of_month => 3,
    reminders    => [10],
  }

}
