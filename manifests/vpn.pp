# Install the base needed to deploy Tails VPN instances.

class tails::vpn (
  Enum['present', 'absent'] $ensure = 'present',
  Array[String] $vpn_nodes          = [],
){

  package { 'tinc':
    ensure  => $ensure,
  }

  concat { '/etc/tinc/nets.boot':
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['tinc'],
  }

  service { 'tinc':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    require    => Package['tinc'],
  }

  concat::fragment { 'nets_boot_debian_comment':
    target  => '/etc/tinc/nets.boot',
    content => "## This file contains all names of the networks to be started on system startup.\n",
    order   => 00,
  }

  $vpn_nodes.each |String $node| {
    file { "/etc/tinc/tailsvpn/hosts/${node}":
      ensure => $ensure,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
      source => "puppet:///modules/tails/vpn/tailsvpn/hosts/${node}"
    }
  }

}
