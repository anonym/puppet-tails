# Manage Tor

class tails::tor {

  # The version of Tor in stretch is not covered by Debian's security support
  # since 2020-03-20 so, in those cases, we pin the version available on
  # backports.

  $ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  apt::pin { 'tor-stretch-backports':
    ensure     => $ensure,
    packages   => ['tor', 'tor-geoipdb'],
    originator => 'Debian Backports',
    codename   => 'stretch-backports',
    priority   => 991,
  }

  include ::tor::daemon

}
