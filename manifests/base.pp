# Base class for team-admin'd systems

class tails::base (
  Boolean $is_puppet_master     = false,
  Boolean $manage_grub          = true,
  Boolean $monitoring_agent     = true,
  String $sshd_tcp_forwarding   = 'no',
  Boolean $has_nvme             = false,
  # Email settings
  Boolean $manage_postfix       = true,
  String $root_mail_recipient   = 'tails-sysadmins@boum.org',
  String $postfix_mydestination = "\$myhostname, ${::fqdn}, localhost",
  String $postfix_smtp_listen   = '127.0.0.1',
  String $postfix_relayhost     = 'direct',
  Boolean $postfix_chroot       = true,
  # systemd settings
  Boolean $systemd_backport     = false,
  # network settings
  Boolean $shorewall            = false,
  Enum['ifupdown', 'systemd-networkd'] $network_manager = 'ifupdown',
  Boolean $systemd_resolved     = false,
) {

  #
  # Sanity checks
  #

  validate_email_address($root_mail_recipient)

  if $::operatingsystem != 'Debian' {
    fail('This module only supports Debian.')
  }

  #
  # Include external classes
  #

  include augeas
  include bash
  include etckeeper
  include haveged
  include loginrecords
  class { '::molly_guard': always_query_hostname => true }
  class { 'sshd':
    tcp_forwarding => $sshd_tcp_forwarding,
  }
  if ! $is_puppet_master {
    if $::fqdn =~ /^isotester[0-9]+\./ {
      include puppet::oneshot
    } else {
      include puppet::cron
    }
  }
  class { 'sudo':
    config_file_replace => false,
  }
  include ::tails::apt
  if $manage_grub {
    include ::tails::grub
  }
  include tails::storage::discard

  # tails::monitoring::* assumes storeconfigs is enabled,
  # so let's not declare any such class if that's not the case
  if $settings::storeconfigs {
    if $monitoring_agent {
      include ::tails::monitoring::agent
    } else {
      # Systems that use tails::monitoring::satellite can't re-declare t::m::agent
      if $::fqdn != 'monitor.lizard' and $::fqdn != 'ecours.tails.boum.org' {
        class { '::tails::monitoring::agent': ensure => absent }
      }
    }
  } else {
    notify { 'storeconfigs is not set => skipping tails::monitoring': }
  }

  case $network_manager {
    'systemd-networkd': {
      include ::tails::systemd::networkd
    }
    default: {}
  }

  if $systemd_resolved {
    if $network_manager == 'systemd-networkd' {
      include ::tails::systemd::resolved
    } else {
      fail("\$systemd_resolved is only supported with \$network_manager == 'systemd-networkd'")
    }
  }

  if $shorewall {
    include ::shorewall
    include ::tails::shorewall
  } else {
    package { ['shorewall', 'shorewall-core']: ensure => purged }
  }

  if $manage_postfix {
    class { '::tails::postfix':
      mydestination       => $postfix_mydestination,
      relayhost           => $postfix_relayhost,
      root_mail_recipient => $root_mail_recipient,
      chroot              => $postfix_chroot,
      smtp_listen         => $postfix_smtp_listen,
    }
  }

  class { '::tails::systemd':
    use_backport => $systemd_backport,
  }

  include ::tails::tor
  include vim

  #
  # Packages
  #

  $base_packages     = [
    apparmor,
    apparmor-profiles-extra,
    apparmor-utils,
    augeas-tools,
    bzip2,
    ca-certificates,
    gnupg,
    gnutls-bin,
    htop,
    iotop,
    iproute2,
    iputils-ping,
    less,
    linux-image-amd64,
    lvm2,
    nload,
    pinentry-curses,
    rsync,
    safe-rm,
    sash,
    screen,
    sysstat,
    tmux,
    torsocks,
    virt-what,
    w3m,
    zsh,
  ]
  $physical_packages = [
    hwloc-nox,
    memlockd,
    'memtest86+',
    numactl,
    parted,
  ]
  $physical_intel_packages = [
    'intel-microcode',
  ]
  $unwanted_packages = [
    acpid,
    apparmor-profiles,
    debian-faq,
    doc-debian,
    icinga2-doc,
    pinentry-gtk2,
    rdiff-backup,
    task-english,
  ]

  ensure_packages($base_packages)
  ensure_packages($unwanted_packages, {'ensure' => 'absent'})

  case $::virtual {
    physical: {
      include smartmontools
      ensure_packages($physical_packages)
      unless $::processor0 =~ /^AMD/ {
        ensure_packages($physical_intel_packages)
      }
    }
    default:  {
      package { 'smartmontools': ensure => absent }
    }
  }

  #
  # Host keys
  #

  sshkey { 'git.tails.boum.org':
    ensure => present,
    type   => 'rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDWZOi/+y7UnMqWyUtQhUCPt6aCt5NzfTPcxS/8AuP00xNKQsPiKRXId2DDYuBf2bp3pDOoxS+S8t1WKJZ7EBoGESYmq5FEx4mSEoK3c+rMv/Phs9phMoOi0xpzn+KpEYBqSo/xzgX35eANTrcnIGx/XwA2QSSehOQPUgAraaQiAEXcNCnDR1M8z6o/JYXXrM2974Uk51082uv+vaxDWpuhw19C6Ytq7P2HRxkRqwu/af8Tn02dea9QjVQ4AJXQNhla2BJrZPEjR4r0fpUjycD0wY4SO/QiLdZNdD21ZayJder96ktsxYbqh219snQsqoMkk60DmCc3dANPgeGHSmJj', # lint:ignore:140chars -- SSH key
  }

  sshkey { 'gitlab-ssh.tails.boum.org':
    ensure => present,
    type   => 'ecdsa-sha2-nistp256',
    key    => 'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBM4AzdtxoBR9EaIOR9yNxeQbdiNdHcqfKUtWP5d46YUer4XAzVxg3x33srMdfk2SNyYP/YllEmm7/+O3yRmBUs4=', # lint:ignore:140chars -- SSH key
  }

  #
  # Miscellaneous settings
  #

  user { 'root': ensure => present }
  file { '/root':
    ensure => directory,
    owner  => root,
    group  => staff,
    mode   => '0750',
  }

  augeas { 'rcS-FSCKFIX':
    context => '/files/etc/default/rcS', changes => 'set FSCKFIX yes';
  }

  sudo::conf {'group-sudo':
    content => "%sudo	ALL=(ALL:ALL) NOPASSWD : ALL",
  }

  sysctl::value { 'kernel.panic': value => 10 }
  sysctl::value { 'kernel.perf_event_paranoid': value => 2 }
  sysctl::value { 'kernel.unprivileged_bpf_disabled': value => 1 }

  class { '::timezone': region => 'Etc', locality => 'UTC' }

  # Use the deadline I/O scheduler for all non-rotational disks
  if $::virtual == 'physical' {
    file { '/etc/systemd/system/tweak-io-scheduler.service':
      owner   => root,
      group   => root,
      mode    => '0644',
      content => '[Unit]
Description=Tweak per-disk block I/O scheduler

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
ExecStart=/bin/sh -c \'for sysblock in /sys/block/nvme* /sys/block/sd* ; do \
                          if [ -d "$sysblock" ] && [ $(cat $sysblock/queue/rotational) = 0 ] ; then \
                             echo deadline > $sysblock/queue/scheduler ; \
                          fi ; \
                       done\'
[Install]
WantedBy=multi-user.target
',
    }
    service { 'tweak-io-scheduler':
      ensure    => running,
      provider  => systemd,
      require   => File['/etc/systemd/system/tweak-io-scheduler.service'],
      subscribe => File['/etc/systemd/system/tweak-io-scheduler.service'],
    }
    exec { 'systemctl enable tweak-io-scheduler.service':
      creates => '/etc/systemd/system/multi-user.target.wants/tweak-io-scheduler.service',
      require => Service[tweak-io-scheduler],
    }
  }

  # Manage CPU frequency settings
  if $::virtual == 'physical' {
    if $::processor0 =~ /^AMD/ {
      class { '::tails::cpufreq': energy_perf_policy => false }
    } else {
      include ::tails::cpufreq
    }
  }

  # Install tools to control NMVe drives when needed
  if $has_nvme {
    ensure_packages(['nvme-cli'])
  }

  file { [
    '/opt/puppetlabs',
    '/opt/puppetlabs/puppet',
    '/opt/puppetlabs/puppet/cache',
    '/opt/puppetlabs/puppet/share',
    '/opt/puppetlabs/puppet/share/augeas',
  ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  package { 'ntp': ensure => absent }
  include ::tails::timesyncd

  # Tor
  tor::daemon::onion_service { 'ssh-hidden-v3':
    ports => [ '22' ],
  }
  tor::daemon::onion_service { 'ssh-hidden':
    ports => [ '22' ],
  }

  # Puppet/etckeeper integration
  file { '/etc/puppet/etckeeper-commit-pre':
    source => 'puppet:///modules/tails/puppet/client/etckeeper-commit-pre',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { '/etc/puppet/etckeeper-commit-post':
    source => 'puppet:///modules/tails/puppet/client/etckeeper-commit-post',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

}
