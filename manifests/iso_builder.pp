# Make a system ready to build Tails ISO and USB images.
class tails::iso_builder (
  Enum['present', 'absent'] $ensure = 'present',
) {

  apt::pin { 'diffoscope':
    ensure     => $ensure,
    packages   => 'diffoscope',
    originator => 'Debian Backports',
    codename   => "${::lsbdistcodename}-backports",
    priority   => 991,
  }

  package { 'diffoscope':
    ensure  => $ensure,
    require => Apt::Pin['diffoscope'],
  }

  $builder_packages = [
    'dnsmasq-base',
    'ebtables',
    'git',
    'libvirt-daemon-system',
    'pigz',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
    'vmdebootstrap'
  ]

  ensure_packages(
    $builder_packages,
    {'ensure' => $ensure}
  )

}
