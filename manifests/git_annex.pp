# Manage the git-annex package
class tails::git_annex (
  Enum['present', 'absent'] $ensure         = present,
  Boolean $use_backport                     = false,
  Variant[Boolean, String] $with_recommends = 'default',
) {

  $install_options = $with_recommends ? {
    false   => [ '--no-install-recommends' ],
    default => [],
  }

  $pin_originator = $use_backport ? {
    true    => 'Debian Backports',
    default => 'Debian',
  }

  $pin_codename = $use_backport ? {
    true    => "${::lsbdistcodename}-backports",
    default => $::lsbdistcodename,
  }

  package { 'git-annex':
    ensure          => $ensure,
    install_options => $install_options,
    require         => Apt::Pin['git', 'git-annex', 'git-man'],
  }

  ['git', 'git-annex', 'git-man'].each |String $package| {
    apt::pin { $package:
      ensure     => $ensure,
      packages   => $package,
      originator => $pin_originator,
      codename   => $pin_codename,
      priority   => 991,
    }
  }

  file { '/usr/local/bin/pull-git-annex':
    ensure => $ensure,
    source => 'puppet:///modules/tails/git-annex/pull-git-annex',
    owner  => root,
    group  => root,
    mode   => '0755',
  }

}
