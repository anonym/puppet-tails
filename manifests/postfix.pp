# Manage Postfix
class tails::postfix (
  String        $root_mail_recipient,
  String        $smtp_listen,
  Boolean       $use_spamassassin = false,
  Boolean       $use_schleuder    = false,
  String        $mydestination    = "\$myhostname, ${::fqdn}, localhost",
  String        $myorigin         = $::fqdn,
  String        $relayhost        = 'direct',
  Array[String] $local_domains    = [],
  Boolean       $chroot           = true,
) {
  class { '::postfix':
    root_mail_recipient => $root_mail_recipient,
    chroot              => $chroot,
    smtp_listen         => $smtp_listen,
    mta                 => true,
    mydestination       => $mydestination,
    myorigin            => $myorigin,
    relayhost           => $relayhost,
    use_amavisd         => $use_spamassassin,
    use_schleuder       => $use_schleuder,
    # IPv6 is disabled on some of our systems
    inet_protocols      => 'ipv4',
    mynetworks          => '127.0.0.0/8',
  }

  ### TLS

  $custom_ssl_dir = '/etc/ssl/postfix'
  file { $custom_ssl_dir:
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['ca-certificates'],
  }

  postfix::config {
    'smtp_tls_CApath':
      value   => '/etc/ssl/certs',
      require => [Package['ca-certificates'], Exec['c_rehash']];
    'smtp_tls_ciphers':              value => 'high';
    'smtp_tls_loglevel':             value => '1';
    'smtp_tls_mandatory_protocols':  value => 'TLSv1.2';
    'smtp_tls_mandatory_ciphers':    value => 'high';
    'smtp_tls_mandatory_exclude_ciphers':
      value => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA';
    'smtp_tls_protocols':            value => 'TLSv1.2';
  }

  $letsencrypt_cafile_basename = 'Lets-Encrypt-Authority-X3.pem'
  $letsencrypt_cafile = "${custom_ssl_dir}/${letsencrypt_cafile_basename}"
  file { $letsencrypt_cafile:
    source => "puppet:///modules/tails/ssl/certs/${letsencrypt_cafile_basename}",
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  postfix::hash { '/etc/postfix/tls_policy':
    ensure  => present,
    content => "# boum.org's MX is Autistici/Inventati, which has valid LE certificates
boum.org                secure tafile=${letsencrypt_cafile} match=.investici.org
# Other Tails SMTPd:s have no valid certificate
tails.boum.org          encrypt
# Tails' main SMTPd has a valid LE certificate
mail.tails.boum.org:25  secure tafile=${letsencrypt_cafile}\n
",
    require => File[$letsencrypt_cafile],
  }
  postfix::config { 'smtp_tls_policy_maps':
    value   => 'hash:/etc/postfix/tls_policy',
  }

  exec { 'c_rehash': refreshonly => true }

  if $chroot {
    # Sync $custom_ssl_dir to the chroot when the service starts,
    # so that we can use its content with tafile= in our TLS policy

    file { '/usr/local/sbin/copy-custom-ssl-certs-to-postfix-chroot.sh':
      ensure  => present,
      content => template('tails/postfix/copy-custom-ssl-certs-to-postfix-chroot.sh.erb'),
      owner   => root,
      group   => root,
      mode    => '0755',
      require => Package[rsync],
    }

    file { '/etc/systemd/system/postfix@.service.d':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755',
    }
    file { '/etc/systemd/system/postfix@.service.d/puppet-tails.conf':
      ensure => present,
      owner  => root,
      group  => root,
      mode   => '0644',
    }

    ini_setting { 'postfix sync custom SSL dir to chroot':
      ensure  => present,
      path    => '/etc/systemd/system/postfix@.service.d/puppet-tails.conf',
      section => 'Service',
      setting => 'ExecStartPre',
      value   => '/usr/local/sbin/copy-custom-ssl-certs-to-postfix-chroot.sh %i',
      notify  => Service['postfix'],
      require => File['/etc/systemd/system/postfix@.service.d/puppet-tails.conf'],
    }
  }

  ### Relay host

  if $relayhost != 'direct' {
    # Send email through a relayhost
    # Note: this class does not manage credentials in sasl_passwd (yet).
    postfix::hash { '/etc/postfix/sasl_passwd': }
    postfix::config { 'smtp_sasl_password_maps':
      value => 'hash:/etc/postfix/sasl_passwd',
    }
    postfix::config { 'smtp_sasl_auth_enable':
      value => 'yes',
    }
    postfix::config { 'smtp_sasl_tls_security_options':
      value => 'noanonymous',
    }
  }

  ### Anti-spam

  if $smtp_listen == 'all' {
    $smtpd_antispam_settings = {
      'smtpd_helo_required'                    => 'yes',
      'smtpd_common_restrictions'              => 'permit_mynetworks, permit_sasl_authenticated',
      'smtpd_client_restrictions'              => '$smtpd_common_restrictions',
      'smtpd_data_restrictions'                => '$smtpd_common_restrictions, reject_unauth_pipelining',
      'smtpd_helo_restrictions'                => '$smtpd_common_restrictions, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname',
      'smtpd_recipient_and_relay_restrictions' => '$smtpd_common_restrictions, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      'smtpd_recipient_restrictions'           => '$smtpd_recipient_and_relay_restrictions',
      'smtpd_relay_restrictions'               => '$smtpd_recipient_and_relay_restrictions',
      'disable_vrfy_command'                   => 'yes',
      'show_user_unknown_table_name'           => 'no',
      'smtpd_etrn_restrictions'                => 'reject',
    }
    $smtpd_antispam_settings.each |String $setting, String $value| {
      postfix::config { $setting: value => $value }
    }

    @@::tails::monitoring::service::smtp { $myorigin:
      real_host => $::fqdn,
    }
  }

  if $use_spamassassin {
    class { '::tails::amavisd_new':
      local_domains => $local_domains,
    }
    include ::tails::spamassassin
    postfix::config { 'content_filter':
      value => 'amavis:[127.0.0.1]:10024',
    }
  }

}
