# Manages the Tails Jenkins master.
# Installs extra packages for nice features as well as Jenkins plugins.
# If $automatic_iso_jobs_generator is 'present', $jenkins_jobs_repo must be
# set to the URL of a jenkins-jobs git repo where the 'jenkins@jenkins-master'
# SshKey has write access. It depends on $deploy_on_git_push being set to true
# for the pushed configuration to be applied automatically.
# For this to happen, hooks managed in tails::gitolite::hooks::jenkins_jobs
# also need to be installed in the jenkins-jobs repo.

class tails::jenkins::master (
  String $jenkins_jobs_repo,
  String $api_token,
  String $version                                         = '2.235.1',

  String $tails_repo                                      = 'https://gitlab.tails.boum.org/tails/tails.git',
  Boolean $deploy_jobs_on_git_push                        = true,
  Enum['present', 'absent'] $automatic_iso_jobs_generator = 'present',
  Integer $active_branches_max_age_in_days                = 49,
  String $gitolite_pubkey_name                            = 'gitolite@puppet-git',
  String $api_user                                        = 'Tails',

  Boolean $manage_mount                                   = false,
  $mount_device                                           = false,
  $mount_fstype                                           = 'ext4',
  $mount_options                                          = 'relatime,user_xattr,acl',

  String $monitoring_parent_zone                          = 'Lizard',
) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $mount_point = '/var/lib/jenkins'
  $ssh_pubkey_name = "jenkins@jenkins-master.${::domain}"

  ### Resources

  apt::pin { 'jenkins':
    packages => 'jenkins',
    version  => $version,
    priority => 991,
  }

  class { 'jenkins':
    repo            => true,
    lts             => true,
    install_java    => false,
    version         => $version,
    default_plugins => [],
    require         => [
      Package[$base_packages],
      Apt::Conf['proxy_jenkins_repo'],
      Apt::Pin['jenkins'],
    ],
  }

  # Provides the jar command, which is used by Jenkins::Cli/Exec[jenkins-cli]
  package { 'openjdk-8-jdk-headless':
    ensure => installed,
  }

  # apt-cacher-ng does not support HTTPS repositories
  apt::conf { 'proxy_jenkins_repo':
    content  => 'Acquire::HTTP::Proxy::pkg.jenkins.io "DIRECT";',
  }
  apt::conf { 'proxy_prodjenkinsreleases_repo':
    content  => 'Acquire::HTTP::Proxy::prodjenkinsreleases.blob.core.windows.net "DIRECT";',
  }

  apt::pin { 'jenkins-job-builder':
    packages   => [
      'jenkins-job-builder',
      'python3-jenkins-job-builder',
      'python3-jenkins'
    ],
    originator => 'Debian Backports',
    priority   => 991,
  }

  $base_packages = [
    'git',
    'jenkins-job-builder',
    'libmockito-java',
    'python3-jenkins-job-builder',
    'python-pkg-resources',
  ]

  ensure_packages($base_packages)

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)
    validate_string($monitoring_parent_zone)

    # Needs to be created by hand before applying this, if $manage_mount
    # is true. We cannot manage File[$mount_point] ourselves as this
    # would duplicate the same declaration in jenkins::config.
    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

    Mount[$mount_point] -> Class['jenkins']

    @@::tails::monitoring::service::disk { "jenkins-data-disk@${::fqdn}":
      nodename  => $::fqdn,
      zone      => $::fqdn,
      partition => $mount_point,
      wfree     => '30000',
      cfree     => '20000',
      tag       => $monitoring_parent_zone,
    }
  }

  include nfs::server

  Nfs::Export <<| tag == $::fqdn |>>

  # lint:ignore:140chars -- SHA512

  jenkins::plugin { 'apache-httpcomponents-client-4-api':
    version       => '4.5.10-2.0',
    digest_string => '86c85b7dfd07b0cc3c5bbde384e8e867935326a42116ee32bcb83d6b0a999334',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'build-symlink':
    version       => '1.1',
    digest_string => 'dc5e517743d872ceefb86199d18c68bef4885c02c275249308a7b37ded8d4504',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'build-timeout':
    version       => '1.19',
    digest_string => 'a92b43adb9c668e3fd0ad307db43c2277cf15ea75c084b5bdc74fb294f80583d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'cloudbees-folder':
    version       => '6.14',
    digest_string => '8b108dd68be1800d502e136dfbdcef123dd2ae35e9026856791fe2bd5875ecf5',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'f09b82ccec2afc60c3b9235d804f312c3e5a0a847b4243b1e3546f718f344af1f7a0f26c4d53a02ae360aa6a50a20676910b143011c4eb880daa8ab0bc0fb073',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'conditional-buildstep':
    version       => '1.3.6',
    digest_string => '4b550bc136fe66bb4eb396605f4036935963327b9c94662f7c441888adb99f77',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'maven-plugin',
      'run-condition',
      'token-macro',
    ],
  }

  jenkins::plugin { 'copyartifact':
    version       => '1.45',
    digest_string => '84548c960e09206d1153f5283d0393d047a0de6edf5d5f6d8a942cd4c93ccf91',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials':
    version       => '2.3.11',
    digest_string => 'a50c27b3c506e60519c7976ec5ff0880c0e29685030f31d3e9883bc69eff02e0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'cucumber-reports':
    version       => '4.10.0',
    digest_string => '1218efe4e476019506bb63e812e8ff88dcdd9bcf11b2caac1d3be56bfff929fe',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'cucumber-testresult-plugin':
    version       => '0.10.1',
    digest_string => '2d2f171a8561ec91a11def39a3f1e75302516f097e86157f5bdd1402f29858bf',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'display-url-api':
    version       => '2.3.2',
    digest_string => 'a4d5f37349930b5dd9a2a5042bd13527b4c2ee316fe49420fe154fa0623a2bb1',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'downstream-ext':
    version       => '1.8',
    digest_string => '5033490d9b34943488e387d64a3a09cf02a43dced29b0f43e8a68b9d837a1869702f9080831fc80e64e2addebf0553bd5c6a8793b46182ed1535422ca839d27f',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'durable-task':
    version       => '1.30',
    digest_string => '580c22a7218b0f3fcd9a0705bba4b919e08d3e23db478342634d8ed8122bc7ed',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'email-ext':
    version       => '2.66',
    digest_string => '65cac44728f454f89eb60c015a239a9eb765108a8c620e3be935bb22e4e60305',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'mailer',
      'matrix-project',
      'script-security',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '2.3.0',
    digest_string => '161010a696fbe1d74a5e2f846794d79e094fae17e8cfa5ffc78810b822cfbc6e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'envinject-api',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'envinject-api':
    version       => '1.7',
    digest_string => '0fb5c4d0b0fbc112addedd604a94d94ce5c42dde3cc6b2494bbf706cdb249a9b',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'git':
    version       => '4.3.0',
    digest_string => 'b85dcef957d7e7a4074b019c36c4e5c4dece1be7cb7af73d641ccf859320e02f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'git-client',
      'mailer',
      'matrix-project',
      'scm-api',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '3.3.0',
    digest_string => 'a60cc8a22b0b3efe42a42ebd9cbe0d2f8d91dd5300bcbcc893e0ce4e424b697c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'credentials',
      'jsch',
      'script-security',
      'ssh-credentials',
      'structs',
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '1.5',
    digest_string => '36b1aeecd6f6cd96263baca7143e4e201f93cc4797b814e62527cf58a9fd4b82',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',
    ],
  }

  jenkins::plugin { 'icon-shim':
    version       => '2.0.3',
    digest_string => 'a83ebce40c28b4bb2474eea3aecfea9c079904b02dee210a70d5ffd7455c437e50ad59b1a9c677a749f9eae84ac702e5d9726a68b63d64ae5c209f5d105418b3',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'javadoc':
    version       => '1.5',
    digest_string => '25514fb702740cbb883fc5c6eeb86177b118517f9b0157f1b2e0c60e8bef1564',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jsch':
    version       => '0.1.55.2',
    digest_string => 'cdc74bf8e43eb40ae6ad98ba2f866c8891408038699da9b836518a1d8923fc44',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'ssh-credentials',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'junit':
    version       => '1.28',
    digest_string => 'a471c80776b9684c4ee9164ce51e01b9871af664bdfce13b11320020ddc25f33',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'script-security',
      'structs',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'mailer':
    version       => '1.32',
    digest_string => '042862b818c72223f1599ac1b95a81f90c312961649e88a6e639c2af8239e4b7',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['display-url-api'],
  }

  jenkins::plugin { 'mapdb-api':
    version       => '1.0.9.0',
    digest_string => '072c11a34cf21f87f9c44bf01b430c5ea77e8096d077e8533de654ef00f3f871',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'matrix-auth':
    version       => '2.6.1',
    digest_string => '5d7dd5ffd60ff691027cbcc17dda63fc2873de2ea2665300f0aa4b2231602f05',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',
    ],
  }

  jenkins::plugin { 'matrix-project':
    version       => '1.14',
    digest_string => '88d84ef75ea63c3ed826caecb2bc03ed59206fd164288f02bade7ce2685a388a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'script-security',
    ],
  }

  jenkins::plugin { 'maven-plugin':
    version       => '3.4',
    digest_string => 'b554ff3395232ddc78f8bf6dd150e8a0994c32a01e16b661d257c6f95d7b44c3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'javadoc',
      'jsch',
      'junit',
      'mailer',
    ],
  }

  jenkins::plugin { 'parameterized-trigger':
    version       => '2.35.2',
    digest_string => '36228ae6c41cf828cf472deabaeff50f8b4e9b69e742deab38d4a9b9a093fd97',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'conditional-buildstep',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'postbuildscript':
    version       => '2.9.0',
    digest_string => '9bd90ecf440ae9f7a2871ae929a8bf309cc078b2f529e4848bb0d0ad66286656',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['matrix-project'],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '3.6.0',
    digest_string => 'a548df16d9a1744c4a5cd2d27c9ed718d14670c346feb916218e2a4960612043',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'resource-disposer':
    version       => '0.13',
    digest_string => 'f0820b7260b7a22aa0c461d8b76cca2140bc15cbe0003650e4821dc99095bb44',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'run-condition':
    version       => '1.2',
    digest_string => '1dbfae6b57c4ae0e190354ac273280bba135aaba82c2d8116bd394c4b83d5e5f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'token-macro',
    ],
  }

  jenkins::plugin { 'scm-api':
    version       => '2.6.3',
    digest_string => '83262d406862ad55ff90d36020a83f88ebd71e66f49c75c9a5140f43176aba29',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'scm-sync-configuration':
    version       => '0.0.10',
    digest_string => '84606ed21b72918a5633cf8e438116ab9af3d9010b2651336af92ce8474e0870',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['subversion'],
  }

  jenkins::plugin { 'script-security':
    version       => '1.74',
    digest_string => '61de31d7aff3c8bac414db9e64e7da7eee0d8a71b80a671a576848a44aa932ad',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'simple-theme-plugin':
    version       => '0.5.1',
    digest_string => 'd823ac7fa1d5861051fc69534f5678c32d10c98a00987a045b0b7836fd733584',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '1.18.1',
    digest_string => '8db908c484737f260cfab6682a42cbeaf390b3b9efd87925375691e45c370492',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'structs':
    version       => '1.20',
    digest_string => '7e7861356a37aa6a727462d7aea716dd9307071252f7349c2726d64a773feb3a',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'subversion':
    version       => '2.13.1',
    digest_string => 'bc36675699051394d4cd7e663631c3c2bdc4668d4da952d0d9d8cd08a308f63c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'mapdb-api',
      'scm-api',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
    ],
  }

  jenkins::plugin { 'timestamper':
    version       => '1.11.3',
    digest_string => '8f44dccc653e03f5c87f04ad146e7be998d3b6cf10300ac6c69ef63870f3603b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'token-macro':
    version       => '2.12',
    digest_string => '05b650913d8f8f65570bdd5e3f396e0bee88ea067abcd2fd789b81428afcf464',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'trilead-api':
    version       => '1.0.8',
    digest_string => '2560952955a7719cba9068a42bd2bfb97dcad4cf083e8eeaf1a784c909203325',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'workflow-api':
    version       => '2.40',
    digest_string => 'd704b80b65589b7148b2fad5c5dcc935cb7a154dac835a61755e3ca39321659d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-durable-task-step':
    version       => '2.34',
    digest_string => 'd1a91f9c175e0f1cf17afdf63c264ab8e66e4370ef1d9822243404091d9dc3bf',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'durable-task',
      'scm-api',
      'script-security',
      'structs',
      'workflow-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'workflow-step-api':
    version       => '2.22',
    digest_string => 'eb9c64a1941d3af320451267e248fe3a0ff9d236e80c3f576f1d3a666d502a33',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'workflow-support':
    version       => '3.3',
    digest_string => '0f2b18d0de9b7c94abc03701e33f660620382dd4fcc83600d6aafcb5888d8f51',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-scm-step':
    version       => '2.11',
    digest_string => '7e0b58f22a7579c937f3081c67c249d4ead678e01a954478fdd59b0cbe3a897c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['workflow-step-api'],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.37',
    digest_string => '9d74adcc911e1b08c7412c23aa55f6aa6d016f000587d255598ce16754d8a90d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'resource-disposer',
      'workflow-durable-task-step',
    ],
  }
  # lint:endignore

  ## Uncomment this (or similar) once all this is moved to a proper class,
  ## that inherits jenkins::service and can thus append to its dependencies.
  # Service['jenkins'] {
  #   require +> File_line['jenkins_HTTP_HOST'],
  # }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  # Used by the reboot_node macro
  file { '/etc/jenkins/jenkins_apikey':
    ensure  => present,
    content => $api_token,
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
  }

  file { '/etc/jenkins_jobs/jenkins_jobs.ini':
    owner   => root,
    group   => jenkins,
    mode    => '0640',
    content => template('tails/jenkins/orchestrator/jenkins_jobs.ini.erb'),
    require => [
      Package['jenkins'],
      Package['jenkins-job-builder'],
    ],
  }

  file { '/etc/jenkins_jobs':
    ensure => directory,
    owner  => root,
    group  => jenkins,
    mode   => '0770',
  }

  vcsrepo { '/etc/jenkins_jobs/jobs':
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => jenkins,
    provider => git,
    source   => $jenkins_jobs_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
      Package['git'],
      File['/etc/jenkins_jobs'],
    ],
  }

  if $deploy_jobs_on_git_push {
    file { '/var/tmp/jenkins_jobs_test':
      ensure => directory,
      owner  => jenkins,
      group  => jenkins,
      mode   => '0700',
    }

    file { '/usr/local/sbin/deploy_jenkins_jobs':
      ensure  => present,
      source  => 'puppet:///modules/tails/jenkins/master/deploy_jenkins_jobs',
      owner   => root,
      group   => root,
      mode    => '0755',
      require => [
        File['/var/tmp/jenkins_jobs_test'],
        Ssh_authorized_key[$gitolite_pubkey_name],
      ],
    }

    sshkeys::set_authorized_keys { $gitolite_pubkey_name:
      user    => jenkins,
      home    => '/var/lib/jenkins',
      require => Package['jenkins'],
    }
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts':
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts',
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts_wrapper':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts_wrapper',
    require => File['/usr/local/bin/clean_old_jenkins_artifacts'],
  }

  cron { 'clean_old_jenkins_artifacts':
    command => '/usr/local/bin/clean_old_jenkins_artifacts_wrapper /var/lib/jenkins',
    user    => 'jenkins',
    hour    => '23',
    minute  => '50',
    require => [File['/usr/local/bin/clean_old_jenkins_artifacts_wrapper'],
                Package['jenkins']],
  }

  file { '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/deduplicate_reproducible_build_jobs_upstream_ISOs',
    require => Package['jenkins'],
  }

  cron { 'deduplicate_reproducible_build_jobs_upstream_ISOs':
    command => '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/6',
    require => File['/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs'],
  }

  file { '/usr/local/bin/manage_latest_iso_symlinks':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/manage_latest_iso_symlinks',
    require => Package['jenkins'],
  }

  cron { 'manage_latest_iso_symlinks':
    command => '/usr/local/bin/manage_latest_iso_symlinks /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/5',
    require => File['/usr/local/bin/manage_latest_iso_symlinks'],
  }

  class  { 'tails::jenkins::iso_jobs_generator':
    ensure            => $automatic_iso_jobs_generator,
    tails_repo        => $tails_repo,
    jenkins_jobs_repo => $jenkins_jobs_repo,
    active_days       => $active_branches_max_age_in_days,
    require           => [
      Class['jenkins'],
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
    ],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => Class['jenkins'],
  }

  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }
}
