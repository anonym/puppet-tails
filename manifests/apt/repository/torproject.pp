class tails::apt::repository::torproject (
  Enum['present', 'absent'] $ensure = 'present',
) {
  apt::source { 'torproject':
    ensure   => $ensure,
    location => 'http://deb.torproject.org/torproject.org',
    release  => $::lsbdistcodename,
    repos    => 'main',
  }
}
