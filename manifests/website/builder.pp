# Manage what's needed to build the Tails website
class tails::website::builder (
  Boolean $with_search = false,
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^stretch|buster|sid$/ {
    fail('This class only supports Debian Stretch, Buster, and sid.')
  }

  ### Resources

  $stretch_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  #6907
  package { 'ikiwiki':
    ensure  => present,
    require => Apt::Pin['ikiwiki'],
  }
  apt::pin { 'ikiwiki':
    ensure     => $stretch_and_older_only_ensure,
    packages   => 'ikiwiki',
    originator => 'Debian',
    codename   => 'buster',
    priority   => 990,
  }

  #17005
  package { 'po4a':
    ensure  => present,
    require => Apt::Pin['po4a'],
  }
  apt::pin { 'po4a':
    ensure     => $stretch_and_older_only_ensure,
    packages   => 'po4a',
    originator => 'Debian Backports',
    codename   => 'stretch-backports',
    priority   => 990,
  }

  $packages = [
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'perlmagick',
    'ruby',
  ]

  ensure_packages($packages)

  if $with_search {
    $search_packages = [
      'libsearch-xapian-perl',
      'xapian-omega',
    ]
    ensure_packages($search_packages)
  }

}
