# Manage Weblate dependencies that are in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::debian_packages () {

  $packages = [
    'ccze', 'ipython3',  # for more convenient debugging
    'mercurial',  # so we can use pip to install from hg repos
    # Dependencies for Weblate 3.8
    'gir1.2-pango-1.0',
    'libcairo2-dev',
    'libgirepository1.0-dev',
    'libglib2.0-dev',
    'pkg-config',
    'python3-chardet',
    'python3-cryptography',
    'python3-dateutil',
    'python3-defusedxml',
    'python3-django-redis',
    'python3-et-xmlfile',
    'python3-hiredis',
    'python3-jdcal',
    'python3-jwt',
    'python3-levenshtein',
    'python3-lxml',
    'python3-memcache',
    'python3-mysqldb',
    'python3-openid',
    'python3-packaging',
    'python3-pil',
    'python3-pyuca',
    'python3-rcssmin',
    'python3-requests',
    'python3-requests-oauthlib',
    'python3-sqlparse',
    'python3-tz',
    'python3-webencodings',
    'python3-whoosh',
    'python3-yaml',
    'sqlite3',
  ]

  ensure_packages($packages)

}
