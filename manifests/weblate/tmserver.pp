# A machine translation service for Weblate
class tails::weblate::tmserver(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $tmserver_data_dir,
  Hash $po_slave_languages,
) {

  # Database

  file { $tmserver_data_dir:
    ensure => directory,
    owner  => tmserver,
    group  => 'weblate',
    mode   => '2771',
  }

  file { "${tmserver_data_dir}/db":
    ensure => present,
    owner  => 'weblate',
    group  => 'tmserver',
    mode   => '0664',
  }

  # User and group

  user { 'tmserver':
    ensure => present,
    system => true,
    home   => $tmserver_data_dir,
    gid    => 'tmserver',
  }

  group { 'tmserver':
    ensure => present,
    system => true,
  }

  # Systemd service

  file { '/usr/local/bin/tmserver':
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => staff,
    require => Tails::Pip_package_from_repo['translate-toolkit'],
  }

  file { '/etc/systemd/system/tmserver.service':
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[
      '/usr/local/bin/tmserver',
      "${tmserver_data_dir}/db",
    ],
    content => "[Unit]
Description=TM Server Instance

[Service]
User=tmserver
Group=tmserver
ExecStart=/usr/local/bin/tmserver -d ${tmserver_data_dir}/db -b 127.0.0.1 -p 8080

[Install]
WantedBy=multi-user.target
",
  }

  service { 'tmserver':
    ensure    => running,
    provider  => systemd,
    require   => [
      File['/etc/systemd/system/tmserver.service']
    ],
    subscribe => File['/etc/systemd/system/tmserver.service'],
  }

  exec { 'systemctl enable tmserver.service':
    creates => '/etc/systemd/system/multi-user.target.wants/tmserver.service',
  }

  # Backups

  file { '/etc/backup.d/20.sh':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "/usr/bin/sqlite3 ${tmserver_data_dir}/db \".backup '/var/backups/tmserver.db'\"",
    require => Package['sqlite3'],
  }

  # Periodically update the server

  file { '/usr/local/bin/build_tmdb':
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => staff,
    require => Tails::Pip_package_from_repo['translate-toolkit'],
  }

  file { "${mutable_data_dir}/scripts/update_tm.sh":
    content => template('tails/weblate/update_tm.sh.erb'),
    mode    => '0750',
    owner   => weblate,
    group   => weblate_admin,
    require => File['/usr/local/bin/build_tmdb'],
  }

  cron { 'weblate update tmserver':
    command  => "${mutable_data_dir}/scripts/update_tm.sh",
    user     => weblate,
    monthday => 1,
    hour     => 5,
    minute   => 23,
    require  => File["${mutable_data_dir}/scripts/update_tm.sh"],
  }

}
