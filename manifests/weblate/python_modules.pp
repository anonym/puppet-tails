# Manage Weblate dependencies that are in not in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::python_modules () {

  $pip_packages = [
    'python3-pip',
  ]

  ensure_packages($pip_packages)

  # Dependencies for Weblate 3.8

  tails::pip_package_from_repo { 'amqp':
    version => '2.6.1',  # >=2.6.0, <2.7
    tag     => 'v2.6.1',
    url     => 'http://github.com/celery/py-amqp',
    require => [Exec['pip_install_vine']],
  }

  tails::pip_package_from_repo { 'asgiref':
    version => '3.2.10',  # ~=3.2.10
    url     => 'https://github.com/django/asgiref/',
  }

  tails::pip_package_from_repo { 'billiard':
    version => '3.6.3.0',  # >=3.6.3.0, <4.0
    url     => 'https://github.com/celery/billiard',
  }

  tails::pip_package_from_repo { 'bleach':
    version => '3.1.5',  # >=3.1.0
    tag     => 'v3.1.5',
    url     => 'https://github.com/mozilla/bleach',
    require => [Exec['pip_install_six']],
  }

  tails::pip_package_from_repo { 'celery':
    version => '4.4.7',  # >=4.0
    tag     => 'v4.4.7',
    url     => 'https://github.com/celery/celery',
    require => [Exec['pip_install_billiard'], Exec['pip_install_kombu'], Exec['pip_install_redis'], Exec['pip_install_vine']],
  }

  tails::pip_package_from_repo { 'celery-batches':
    version => '0.3',  # >=0.2
    tag     => 'v0.3',
    url     => 'https://github.com/percipient/celery-batches',
    require => [Exec['pip_install_celery']],
  }

  tails::pip_package_from_repo { 'diff-match-patch':
    version => '20181111',  # ==20181111
    tag     => 'v20181111',
    url     => 'https://github.com/diff-match-patch-python/diff-match-patch',
  }

  tails::pip_package_from_repo { 'django':
    version => '2.2.16',  # >=1.11, <3.0
    url     => 'https://github.com/django/django',
    require => [Exec['pip_install_asgiref']],
  }

  tails::pip_package_from_repo { 'django-appconf':
    version => '1.0.4',  # >=1.0
    tag     => 'v1.0.4',
    url     => 'https://github.com/django-compressor/django-appconf',
    require => [Exec['pip_install_django']],
  }

  tails::pip_package_from_repo { 'django-compressor':
    version => '2.4',  # >=2.1.1
    url     => 'https://github.com/django-compressor/django-compressor',
    require => [Exec['pip_install_django-appconf'], Exec['pip_install_rjsmin'], Exec['pip_install_six']],
  }

  tails::pip_package_from_repo { 'django-crispy-forms':
    version => '1.9.2',  # >=1.6.1
    url     => 'https://github.com/django-crispy-forms/django-crispy-forms',
  }

  tails::pip_package_from_repo { 'djangorestframework':
    version => '3.11.1',  # >=3.8
    url     => 'https://github.com/encode/django-rest-framework',
    require => [Exec['pip_install_django']],
  }

  tails::pip_package_from_repo { 'filelock':
    version => '3.0.12',  # >=3.0.1
    tag     => 'v3.0.12',
    url     => 'https://github.com/benediktschmitt/py-filelock',
  }

  tails::pip_package_from_repo { 'html2text':
    version => '2020.1.16',  # >=2018.1.9
    url     => 'https://github.com/Alir3z4/html2text/',
  }

  tails::pip_package_from_repo { 'importlib-metadata':
    version => '1.7.0',  # >=0.18
    tag     => 'v1.7.0',
    url     => 'https://gitlab.com/python-devs/importlib_metadata',
    require => [Exec['pip_install_zipp']],
  }

  tails::pip_package_from_repo { 'jellyfish':
    version => '0.8.2',  # >=0.6.1
    url     => 'https://github.com/jamesturk/jellyfish',
  }

  tails::pip_package_from_repo { 'kombu':
    version => '4.6.11',  # >=4.6.10, <4.7
    tag     => 'v4.6.11',
    url     => 'http://github.com/celery/kombu',
    require => [Exec['pip_install_amqp'], Exec['pip_install_importlib-metadata'], Exec['pip_install_redis']],
  }

  tails::pip_package_from_repo { 'oauthlib':
    version => '3.1.0',  # >=3.0.0
    tag     => 'v3.1.0',
    url     => 'https://github.com/oauthlib/oauthlib',
  }

  tails::pip_package_from_repo { 'openpyxl':
    version   => '2.6.4',  # >=2.6.0, <3.0
    url       => 'https://foss.heptapod.net/openpyxl/openpyxl',
    repo_type => 'hg',
  }

  tails::pip_package_from_repo { 'pycairo':
    version => '1.19.1',  # >=1.10.0, >=1.11.1
    tag     => 'v1.19.1',
    url     => 'https://github.com/pygobject/pycairo',
  }

  tails::pip_package_from_repo { 'pygobject':
    version => '3.36.1',  # >=3.14.0
    url     => 'https://gitlab.gnome.org/GNOME/pygobject.git',
  }

  tails::pip_package_from_repo { 'python3-openid':
    version => '3.2.0',  # >=3.0.10
    tag     => 'v3.2.0',
    url     => 'http://github.com/necaris/python3-openid',
  }

  tails::pip_package_from_repo { 'redis':
    version => '3.5.3',  # >=3.2.0
    url     => 'https://github.com/andymccurdy/redis-py',
  }

  tails::pip_package_from_repo { 'rjsmin':
    version => '1.1.0',  # ==1.1.0
    url     => 'https://github.com/ndparker/rjsmin',
  }

  tails::pip_package_from_repo { 'ruamel.yaml':
    version   => '0.16.12',  # >=0.15.0
    url       => 'http://hg.code.sf.net/p/ruamel-yaml/code',
    repo_type => 'hg',
  }

  tails::pip_package_from_repo { 'setuptools':
    version => '49.2.1',  # >49.2
    tag     => 'v49.2.1',
    url     => 'https://github.com/pypa/setuptools',
  }

  tails::pip_package_from_repo { 'siphashc':
    version => '2.1',  # >=0.8
    tag     => 'v2.1',
    url     => 'https://github.com/WeblateOrg/siphashc',
  }

  tails::pip_package_from_repo { 'six':
    version => '1.15.0',  # >=1.7.0, >=1.12.0
    url     => 'https://github.com/benjaminp/six',
  }

  tails::pip_package_from_repo { 'social-auth-app-django':
    version => '4.0.0',  # >=3.1.0
    url     => 'https://github.com/python-social-auth/social-app-django',
    require => [Exec['pip_install_six'], Exec['pip_install_social-auth-core']],
  }

  tails::pip_package_from_repo { 'social-auth-core':
    version => '3.3.3',  # >=3.1.0
    url     => 'https://github.com/python-social-auth/social-core',
    require => [Exec['pip_install_oauthlib'], Exec['pip_install_python3-openid'], Exec['pip_install_six']],
  }

  tails::pip_package_from_repo { 'translate-toolkit':
    version => '2.4.0',  # ==2.4.0
    url     => 'https://github.com/translate/translate',
  }

  tails::pip_package_from_repo { 'translation-finder':
    version => '2.1',  # >=1.4
    url     => 'https://github.com/WeblateOrg/translation-finder.git',
    require => [Exec['pip_install_ruamel.yaml']],
  }

  tails::pip_package_from_repo { 'ua-parser':
    version => '0.10.0',  # >=0.10.0
    url     => 'https://github.com/ua-parser/uap-python',
  }

  tails::pip_package_from_repo { 'user-agents':
    version => '2.2.0',  # >=1.1.0
    tag     => 'v2.2.0',
    url     => 'https://github.com/selwin/python-user-agents',
    require => [Exec['pip_install_ua-parser']],
  }

  tails::pip_package_from_repo { 'vine':
    version => '1.3.0',  # ==1.3.0
    tag     => 'v1.3.0',
    url     => 'http://github.com/celery/vine',
  }

  tails::pip_package_from_repo { 'zipp':
    version => '1.2.0',  # >=0.5, ==1.2.0
    tag     => 'v1.2.0',
    url     => 'https://github.com/jaraco/zipp',
  }

}
