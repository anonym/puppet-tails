#!/usr/bin/env python3

"""
This script reads the dependency tree of a specific version of Weblate and
builds Puppet manifests that declare Debian and Python packages. Extra
requirements are not taken into account when building such dependencies lists.
"""

from bs4 import BeautifulSoup
from operator import attrgetter
from packaging import version as packaging_version
from string import Template
import argparse
import git
import io
import itertools
import pathlib
import re
import requests
import urllib
import yaml

from debian.debian_support import Version

import apt_pkg
apt_cache = apt_pkg.Cache()

class Dependency:

    def __init__(self, line, config, split=True):
        self.line = line
        self.config = config
        self.python_version = None
        self._pypi = None
        self._tag = None
        self.comment = bool(re.match("^\s*#", line))
        if self.comment:
            return
        if ";" in line:
            m = re.search(r"; python_version ([<>=]+) '([0-9.]+)'", line)
            if m:
                self.python_version = Version(m.group(2))
                self.python_constrain = m.group(1)
            line = line[:line.find(";")].strip()
        self.dep = line
        first = True
        self.constrains=set()

        if split:
            for part in line.split(","):
                m = re.match(r"^(?P<name>[^!=<>]*)\s*(?P<constrains>[!=<>~0-9abrc\.dev,]+)?\s*$", part)
                if first:
                    self.name = m.group("name")
                    first = False
                if m.group("constrains"):
                     for i in self.parse_constrains(m.group("constrains")):
                         self.constrains.add(i)
        else:
            m = re.match(r"^(?P<name>[^!=<>]*)\s*(?P<constrains>\([!=<>~0-9abrc\.dev,]+\))?\s*$", line)
            self.name = m.group("name")
            if m.group("constrains"):
                for i in self.parse_constrains(m.group("constrains")[1:-1]):
                    self.constrains.add(i)
        self.name = self.name.lower().strip().replace("_","-")
        if self.name in self.config:
            if "additional_condition" in self.config[self.name]:
                for i in self.config[self.name]["additional_condition"]:
                    for j in self.parse_constrains(i):
                        self.constrains.add(j)


    def parse_constrains(self, constrains):
        for part in constrains.split(","):
            n = re.match(r"^(?P<constrain>[!=<>~]+)\s*(?P<version>[0-9][0-9abrc\.]*|dev)$", part.strip())
            yield (n.group("constrain"), n.group("version"))


    def pypi(self):
        if self._pypi:
            return self._pypi
        print(f"  Fetching data for {self.name}...")
        ret = requests.get(f"https://pypi.org/pypi/{self.name}/json")
        ret.raise_for_status()
        self._pypi = ret.json()
        return self._pypi


    def python_match(self, ver):
        if self.comment:
            return False
        if not self.python_version:
            return True
        if self.python_constrain == ">":
            return ver > self.python_version
        elif self.python_constrain == "<":
            return ver < self.python_version
        elif self.python_constrain == ">=":
            return ver >= self.python_version
        else:
            print(f"Unknown constrain: {self.python_constrain}, {self.line}")

    def match(self, version):
        if self.comment:
            return False
        for constrain in self.constrains:
            c = constrain[0]
            v = constrain[1]
            if c == "==":
                if not Version(version.upstream_version) == Version(v):
                    return False
            elif c == ">=":
                if not Version(version.upstream_version) >= Version(v):
                    return False
            elif c == "<=":
                if not Version(version.upstream_version) <= Version(v):
                    return False
            elif c == "<":
                if not Version(version.upstream_version) < Version(v):
                    return False
            elif c == "!=":
                if not Version(version.upstream_version) != Version(v):
                    return False
        return True

    @property
    def text_constrains(self):
        strconstrains = []
        for constrain in self.constrains:
            strconstrains.append(constrain[0]+constrain[1])
        return ", ".join(sorted(strconstrains, key=lambda s: packaging_version.parse(re.sub('[<>=]', '', s))))

    def __repr__(self):
        if self.comment:
            return f"<Dependency comment('{self.line}')>"

        if self.python_version:
            return f"<Dependency {self.name}({self.text_constrains}) (only for Python {self.python_constrain} {self.python_version})>"
        else:
            return f"<Dependency {self.name}({self.text_constrains})>"

    @property
    def url(self):
        if self.name in self.config:
            if "url" in self.config[self.name]:
                return self.config[self.name]["url"]

        return self.pypi()['info']['home_page']

    @property
    def newest_pip_version(self):
        pipy_version = self.pypi()['info']['version']
        if not self.match(Version(pipy_version)):
            versions = filter(lambda i: self.match(Version(i)),self.pypi()['releases'].keys())
            sorted_versions = sorted(versions,key=lambda i:Version(i), reverse=True)
            return sorted_versions[0]
        return pipy_version

    @property
    def tag(self):
        if self._tag:
            return self._tag
        print(f"  Fetching tags for {self.name}")
        urls = [
                f"{self.url}/tags",    # Github tag page
                f"{self.url}/-/tags",  # Gitlab tag page
        ]
        for url in urls:
            r = requests.get(f"{self.url}/tags")
            if r.status_code == 200:
                break
        else:
            self._tag = None
            return self._tag
        if r.request.path_url.endswith("-/tags"):  # look at final URL path in case of redirection
            #gitlab
            url_path = urllib.parse.urljoin(urllib.parse.urlparse(self.url).path+"/", "-/tags/")
        else:
            #github
            url_path = urllib.parse.urljoin(urllib.parse.urlparse(self.url).path+"/", "releases/tag/")
        soup = BeautifulSoup(r.text, 'html.parser')
        links = soup.find_all('a')
        ll = [i.get("href") for i in links if i.get("href") is not None and i.get("href").startswith(url_path)]
        versions = [self.newest_pip_version]
        if self.newest_pip_version.endswith(".0"):
            versions.append(self.newest_pip_version[:-2])
        for v in versions:
            try:
                # match URLs ending in this specific version, but also account
                # for a possibly longer version string.
                filter_fun = lambda i: re.match('.*/v?{}[0-9.]*$'.format(v), i)
                version_tag = next(filter(filter_fun, ll)).split("/")[-1]
                break
            except StopIteration:
                pass
        else:
            self._tag = None
            return self._tag

        if version_tag == self.newest_pip_version:
            self._tag = None
            return self._tag
        else:
            self._tag = version_tag
            return self._tag

    @property
    def repo_type(self):
        if self.name in self.config:
            if "repo_type" in self.config[self.name]:
                return self.config[self.name]["repo_type"]
        return None

    def require(self, via_pip):
        if not self.pypi()['info']['requires_dist']:
            return []

        for i in self.pypi()['info']['requires_dist']:
            if "; extra == 'redis'" in i:
                i = i[:i.find("; extra")].strip()
            if  " extra ==" in i:
                continue
            dep = Dependency(i, self.config, split=False)
            if via_pip and dep.name not in via_pip:
                continue
            yield dep

    def puppet_code(self, via_pip):
        head = f"tails::pip_package_from_repo {{ '{self.name}':"
        tail = "  }"
        lines = []
        if self.text_constrains:
            lines.append(f"version => '{self.newest_pip_version}',  # {self.text_constrains}")
        else:
            lines.append(f"version => '{self.newest_pip_version}',")

        if self.tag:
            lines.append(f"tag     => '{self.tag}',")

        lines.append(f"url     => '{self.url}',")

        if self.repo_type:
            lines.append(f"repo_type => '{self.repo_type}',")

        deps = list(map(lambda d: f"Exec['pip_install_{d.name}']", self.require(via_pip)))
        if deps:
            lines.append(f"require => [{', '.join(sorted(deps))}],")

        inner = '\n    '.join(lines)

        return f"{head}\n    {inner}\n{tail}\n"

class DebianSuite:
    def stretch(v):
        for f in v.file_list:
            if f[0].site != 'deb.debian.org':
                continue
            archive = f[0].archive
            if archive.startswith("oldstable"):
                return archive
            elif archive.startswith("stretch"):
                return archive
        else:
            return False

    def buster(v):
        for f in v.file_list:
            if f[0].site != 'deb.debian.org':
                continue
            archive = f[0].archive
            if archive.startswith("stable"):
                return archive
            elif archive.startswith("buster"):
                return archive
        else:
            return False

    def bullseye(v):
        for f in v.file_list:
            if f[0].site != 'deb.debian.org':
                continue
            archive = f[0].archive
            if archive.startswith("testing"):
                return archive
            elif archive.startswith("bullseye"):
                return archive
        else:
            return False

def names(basename):
    if basename.startswith("python"):
        yield from names(basename[basename.find("-")+1:])
    elif basename.startswith("py"):
        yield from names(basename[2:])
    yield "python3-"+basename
    yield basename

def deb_versions(pkgname,suite):
  err = None
  once = False
  for n in names(pkgname.lower()):
    try:
       a_pkg = apt_cache[n]
    except KeyError as e:
       if not err:
         err = e
       continue
    for v in a_pkg.version_list:
       archive = suite(v)
       if not archive:
           continue
       yield Version(v.ver_str), archive, v
       once = True
    if once:
        return
  else:
       raise err

def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--weblate-version",
        help="What Weblate version.",
        required=True)

    parser.add_argument(
        "--debian-suite",
        choices=['stretch', 'buster', 'bullseye'],
        help="Debian suite.",
        required=True)

    parser.add_argument(
        "--weblate-repo",
        help="Path to Weblate repository.",
        required=True)

    parser.add_argument(
        "--config-file",
        help="Path to configuration file.",
        default=str(pathlib.Path(__file__).with_name("override.yaml")))

    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()

    suite_name = args.debian_suite
    suite = getattr(DebianSuite, suite_name)

    # Find Python version

    a_pkg = apt_cache['python3']
    for v in a_pkg.version_list:
        archive = suite(v)
        if not archive:
            continue
        running_python = Version(v.ver_str)
        break

    # Get requirements from Weblate Git repository

    repo = git.Repo(args.weblate_repo)

    output = io.BytesIO()
    b = repo.commit("weblate-{}".format(args.weblate_version)).tree/"requirements.txt"
    b.stream_data(output)

    config = yaml.safe_load(pathlib.Path(args.config_file).open())

    SKIP_LIST = set(config.get('skip'))
    PIP_ONLY = set(config.get('require_pip'))

    pip_needs = set()
    debian_needs = set(config.get('require_debian'))

    deps = [Dependency(i, config['pip']) for i in output.getvalue().decode().splitlines()]
    deps += [Dependency(i, config['pip']) for i in config.get('extras_pip', [])]

    def in_debian(dep):
        try:
            debversions = list(deb_versions(dep.name, suite))
            if not debversions:
                return False
            for version, archive, pkg in debversions:
                if dep.match(version):
                    return pkg.parent_pkg.name
        except KeyError:
            return False

    print("Adding first dependencies...")

    known = set()

    for dep in deps:
        if dep.comment:
            continue
        known.add(dep.name)
        if not dep.python_match(running_python.upstream_version):
            continue
        if dep.name in SKIP_LIST:
            continue
        if dep.name in PIP_ONLY:
            pip_needs.add(dep)
            continue
        debian_package = in_debian(dep)

        if debian_package:
            debian_needs.add(debian_package)
        else:
            pip_needs.add(dep)

    print("Traversing dependency tree...")

    # Traverse dependency many times adding new deps until no new dep is
    # added

    add = True
    while add:
        add = False
        pip_adds = set()
        for dep in pip_needs:
            try:
                for require in dep.require([]):
                    if require.comment:
                        continue
                    if require.name in known:
                        continue
                    known.add(require.name)

                    if not require.python_match(running_python.upstream_version):
                        continue
                    if require.name in SKIP_LIST:
                        continue
                    if require.name in PIP_ONLY:
                        pip_adds.add(require)
                        continue
                    debian_package = in_debian(require)

                    if debian_package:
                        debian_needs.add(debian_package)
                    elif require not in pip_needs:
                        pip_adds.add(require)
                        add = True
            except requests.HTTPError as e:
                print(e)
        pip_needs |= pip_adds

    # Write Puppet manifest for Debian packages

    print("Writing Debian packages manifest...")

    script_path = pathlib.Path(__file__).parent
    target_path = script_path.joinpath('../../')

    template_file = script_path.joinpath("debian_packages.pp.tpl").read_text()
    template = Template(template_file)
    debian_depends = template.substitute(
            WEBLATE_DEPENDS="',\n    '".join(sorted(debian_needs)),
            WEBLATE_VERSION= args.weblate_version,
            )
    target_path.joinpath("manifests/weblate/debian_packages.pp").write_text(debian_depends)

    print("...finished.")

    # Write Puppet manifest for Python packages

    print("Writing Python packages manifest...")

    template_file = script_path.joinpath("python_modules.pp.tpl").read_text()
    template = Template(template_file)
    via_pip = set((i.name for i in pip_needs))
    pip_depends = []
    for dep in sorted(pip_needs, key=attrgetter('name')):
        try:
            pip_depends.append(dep.puppet_code(via_pip))
        except requests.HTTPError:
            print(f"Didn't find {dep.name} on pypi.")

    python_modules = template.substitute(
            PIP_DEPENDS="\n  ".join(pip_depends),
            WEBLATE_VERSION= args.weblate_version,
            )
    target_path.joinpath("manifests/weblate/python_modules.pp").write_text(python_modules)

    print("...finished.")


if __name__ == "__main__":
    main()
