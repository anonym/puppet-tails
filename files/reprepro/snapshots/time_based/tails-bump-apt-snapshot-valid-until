#!/bin/sh
set -e
set -u

error() {
   echo "$*" >&2
   exit 1
}

if [ $# -lt 3 ]; then
	error "E: $0 archive serial days (not enough parameters)"
fi

archive="$1"
serial="$2"
days="$3"

if [ ! -d "repositories/$archive" ]; then
	error "E: archive $archive not found under repositories/"
fi

# Make it valid for the specified amount of days:
validity=$(date --utc --rfc-2822 -d "now + $days days")

# Find snapshots for that serial for all suites (jessie, jessie/updates, etc.):
release_files=$(find "repositories/$archive/dists/" -wholename "*/snapshots/$serial/Release")
if [ -z "$release_files" ]; then
	error "E: no Release files under snapshots/$serial, buggy serial?"
fi
for release in $release_files; do
	# Have a chance to detect we went too far (e.g. binary-amd64/Release):
	if ! grep -qs ^Date: "$release"; then
		error "Field 'Date' not found in Release file $release, aborting"
	fi

	# Update field if present, add after Date otherwise:
	if grep -qs ^Valid-Until: "$release"; then
		sed -i "s/^Valid-Until:.*$/Valid-Until: $validity/" "$release"
	else
		sed -i "/^Date:/a Valid-Until: $validity" "$release"
	fi

	# Generate inline signature:
	inrelease="${release%%Release}InRelease"
	rm -f "$inrelease"
	gpg --digest-algo SHA512 --output "$inrelease" --clearsign "$release"

	# Generate detached signature:
	gpgrelease="${release}.gpg"
	rm -f "$gpgrelease"
	gpg --digest-algo SHA512 --armor --output "$gpgrelease" --detach-sign "$release"
done
