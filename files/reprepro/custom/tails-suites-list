#!/bin/sh

set -e

GIT_REPO="$1"

### Functions

. /usr/local/share/tails-reprepro/functions.sh

### Sanity checks

[ -n "$GIT_REPO" ] || error "Usage: $(basename $0) GIT_REPO"
[ -d "$GIT_REPO" ] || error "Usage: $(basename $0) GIT_REPO"

### Main

cd "$GIT_REPO"

# Git branches
git branch --color=never | sed -e 's,^..,,' | (
   while read branch ; do
      suite=$(ref_name_to_suite "$branch")
      echo "'${suite}':"
   done
)

# Git tags
git tag | (
   while read tag ; do
      if echo "$tag" | grep -qs -E '^jenkins-(build|fetch)_'; then
	 continue
      fi
      suite=$(ref_name_to_suite "$tag")
      echo "'${suite}':"
      echo "  released: true"
   done
)

# Other suites
echo "'asp-test-upgrade-cowsay':"
echo "'bitcoind-stretch':"
echo "'builder-bullseye':"
echo "'builder-jessie':"
echo "'builder-stretch':"
echo "'gitolite2':"
echo "'icingaweb2-jessie':"
echo "'ikiwiki':"
echo "'isotester-bullseye':"
echo "'isotester-buster':"
echo "'isotester-stretch':"
echo "'iukbuilder-stretch':"
echo "'jenkins-master':"
echo "'jenkins-slave':"
echo "'playground':"
echo "'puppetmaster-jessie':"
echo "'puppet3x':"
echo "'puppetdb':"
