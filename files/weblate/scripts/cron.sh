#!/bin/sh

set -e
set -u

PIDFILE=/tmp/weblate-cronjob.pid

INTEGRATION_GIT_CHECKOUT=/var/lib/weblate/repositories/integration
WEBLATE_GIT_CHECKOUT=/var/lib/weblate/repositories/vcs/tails/index
WEBLATE_CODE_GIT_CHECKOUT=/usr/local/share/weblate
UPDATE_LOGFILE=/var/log/weblate/update.log

log() {
   echo "$(date '+%F %T') - cron.sh - ${*}" >> "$UPDATE_LOGFILE"
}

git_fetch() {
  checkout="$1"
  remote="$2"

  log "Updating '${remote}' remote of in '${checkout}'..."
  (
    cd "$checkout"
    git fetch "$remote" >> "$UPDATE_LOGFILE" 2>&1
    log "done."
  )
}

git_push() {
   checkout="$1"
   remote="$2"
   branch="$3"

  log "Pushing ${branch} branch of '${checkout}' to the '${remote}' remote..."
  (
    cd "${checkout}"
    git push --quiet ${remote} ${branch} >> "$UPDATE_LOGFILE" 2>&1
    log "done."
  )
}


docron() {
  log "Merging changes from main repo into the integration repo..."
  git_fetch "${INTEGRATION_GIT_CHECKOUT}" origin
  /var/lib/weblate/scripts/merge_canonical_changes.py \
    "${INTEGRATION_GIT_CHECKOUT}" origin/master
  log "done."

  git_push "${INTEGRATION_GIT_CHECKOUT}" weblate-gatekeeper master

  log "Fetching and merging changes made by Weblate into the integration repo..."
  git_fetch "${INTEGRATION_GIT_CHECKOUT}" weblate
  /var/lib/weblate/scripts/merge_weblate_changes.py \
    "${INTEGRATION_GIT_CHECKOUT}" weblate/master
  log "done."

  git_push "${INTEGRATION_GIT_CHECKOUT}" weblate-gatekeeper master

  git_fetch "${WEBLATE_GIT_CHECKOUT}" origin

  log "Updating Weblate components in Weblate repo..."
  /var/lib/weblate/scripts/update_weblate_components.py \
    --remoteBranch=cron/master \
    --fetch \
    "${WEBLATE_GIT_CHECKOUT}"
  log "done."

  log "Updating Core Pages component list..."
  /var/lib/weblate/scripts/update_core_pages.py
  log "done."
}

if ! pgrep --pidfile="$PIDFILE" >/dev/null 2>&1; then
  echo $$ > "$PIDFILE"
  log "Starting docron run..."
  docron
  log "Completed docron run."
  rm "$PIDFILE"
fi
