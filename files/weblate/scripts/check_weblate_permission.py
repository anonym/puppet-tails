#!/usr/bin/env python3

# Check expected permissions for Weblate against actually configured ones.

import yaml
import tailsWeblate
from weblate.auth.models import User, Role, Group, Permission


EXPECTED_PERMISSIONS = "/var/lib/weblate/config/weblate_permission.yaml"


def check_permissions():

    with open(EXPECTED_PERMISSIONS) as f:
        expected = yaml.load(f)

    for i in Role.objects.all():
        if i.name in expected["roles"]:
            real = [j.codename for j in i.permissions.all()]
            if real != expected["roles"][i.name]:
                print("Role mismatch for {name}".format(name=i.name))

    for i in Group.objects.all():
        if i.name in expected["groups"]:
            real = [j.name for j in i.roles.all()]
            if real != expected["groups"][i.name]:
                print("Group mismatch for {name}".format(name=i.name))

    for u in User.objects.all():
        for group in u.groups.all():
            if group.name not in expected["groups"]:
                print("user {username} is not allowed to be in {group}".format(username=u.username, group=group.name))
        expected_superuser = u.username in expected["superusers"]
        if expected_superuser != u.is_superuser:
            print("Wrong superuser status for {username}".format(username=u.username))


if __name__ == "__main__":
    check_permissions()
