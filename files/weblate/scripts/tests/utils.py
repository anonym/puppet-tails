import git
import os
import pathlib
import shutil


def createRepo(basepath):
    datapath = pathlib.Path(os.path.realpath(__file__)).parent / "data"
    repopath = os.path.join(basepath, "repo")
    try:
        os.mkdir(basepath)
    except FileExistsError:
        pass
    try:
        os.mkdir(repopath)
    except FileExistsError:
        pass
    try:
        os.mkdir(os.path.join(repopath,"wiki"))
    except FileExistsError:
        pass
    try:
        os.mkdir(os.path.join(repopath,"wiki/src"))
    except FileExistsError:
        pass

    sub = git.Repo.init(os.path.join(basepath, "sub"))
    open(os.path.join(basepath,"sub/subfile"), "w").write("")
    sub.index.add(["subfile"])
    sub.index.commit("first commit")
    sub.create_tag('base-1')

    r = git.Repo.init(repopath)

    open(os.path.join(repopath,"a.mdwn"), "w").write("")

    r.index.add(["a.mdwn"])
    git.Submodule.add(r, "subtest", "submodules/test", url="file://"+os.path.join(basepath, "sub"))
    r.index.commit("first commit")
    r.create_tag('base-1')

    master = r.refs['master']
    submodule = r.submodule('subtest')

    master.commit = r.commit("base-1")
    master.checkout(force=True)
    open(os.path.join(repopath,"submodules/test/subfile"), "w").write("blub")
    submodule.module().index.add(["subfile"])
    submodule.module().index.commit("changed subfile")
    submodule.binsha = submodule.module().head.commit.binsha
    r.index.add([submodule])
    r.index.commit("submodule change")
    r.create_tag('submodule-change')

    # reset submodule again
    master.commit = r.commit("base-1")
    submodule.module().refs['master'].commit = submodule.module().commit('base-1')
    submodule.module().refs['master'].checkout(force=True)

    master.commit = r.commit("base-1")
    master.checkout(force=True)
    #r.submodule_update(force_reset=True)
    shutil.copy(str(datapath/"a.fa.po.localchanges"), os.path.join(repopath, "a.fa.po"))
    r.index.add(["a.fa.po"])
    r.index.commit("farsi local changes")
    r.create_tag('master-localchanges')

    master.commit = r.commit("base-1")
    master.checkout(force=True)
    shutil.copy(str(datapath/"a.de.po.base"), os.path.join(repopath, "a.de.po"))
    shutil.copy(str(datapath/"a.fr.po.base"), os.path.join(repopath, "a.fr.po"))
    r.index.add(["a.de.po", "a.fr.po"])
    r.index.commit("add po files")
    r.create_tag('remote-1')
    r.create_head('remote')
    remoteHead = r.refs['remote']

    master.commit = r.commit("remote-1")
    master.checkout(force=True)
    shutil.copy(str(datapath/"a.de.po.update"), os.path.join(repopath, "a.de.po"))
    r.index.add(["a.de.po"])
    r.index.commit("update po file")
    r.create_tag('remote-update')

    master.commit = r.commit("remote-1")
    master.checkout(force=True)
    shutil.copy(str(datapath/"b.de.po.add"), os.path.join(repopath, "b.de.po"))
    open(os.path.join(repopath,"b.mdwn"), "w").write("")
    r.index.add(["b.de.po", 'b.mdwn'])
    r.index.commit("add b component")
    r.create_tag('remote-add')

    master.commit = r.commit("remote-1")
    master.checkout(force=True)
    shutil.copy(str(datapath/"index.de.po.addWiki"), os.path.join(repopath, "wiki/src/index.de.po"))
    shutil.copy(str(datapath/"index.fr.po.addWiki"), os.path.join(repopath, "wiki/src/index.fr.po"))
    open(os.path.join(repopath,"wiki/src/index.mdwn"), "w").write("")
    r.index.add(["wiki/src/index.mdwn", "wiki/src/index.de.po", "wiki/src/index.fr.po"])
    r.index.commit("add wiki files")
    r.create_tag('remote-addWiki')
    r.create_tag('remote-wiki-add')

    master.commit = r.commit("remote-wiki-add")
    master.checkout(force=True)
    shutil.copy(str(datapath/"index.de.po.wiki-update"), os.path.join(repopath, "wiki/src/index.de.po"))
    r.index.add(["wiki/src/index.de.po"])
    r.index.commit("update wiki files")
    r.create_tag('remote-wiki-update')

    master.commit = r.commit("remote-wiki-add")
    master.checkout(force=True)
    shutil.copy(str(datapath/"index.de.po.wiki-update2"), os.path.join(repopath, "wiki/src/index.de.po"))
    r.index.add(["wiki/src/index.de.po"])
    r.index.commit("update wiki files")
    r.create_tag('remote-wiki-update2')

    master.commit = r.commit("remote-wiki-add")
    master.checkout(force=True)
    r.index.remove(["wiki/src/index.mdwn", "wiki/src/index.de.po", "wiki/src/index.fr.po"])
    r.index.commit("delete")
    r.create_tag('remote-wiki-delete')

    master.commit = r.commit("remote-wiki-add")
    master.checkout(force=True)
    r.index.remove(["wiki/src/index.mdwn", "wiki/src/index.de.po", "wiki/src/index.fr.po"])
    r.index.commit("delete2")
    r.create_tag('remote-wiki-delete2')

    master.commit = r.commit("remote-wiki-add")
    master.checkout(force=True)
    os.rename(os.path.join(repopath, "wiki/src/index.de.po"), os.path.join(repopath, "wiki/src/new.de.po"))
    os.rename(os.path.join(repopath, "wiki/src/index.fr.po"), os.path.join(repopath, "wiki/src/new.fr.po"))
    os.rename(os.path.join(repopath, "wiki/src/index.mdwn"), os.path.join(repopath, "wiki/src/new.mdwn"))
    r.index.remove(["wiki/src/index.mdwn", "wiki/src/index.de.po", "wiki/src/index.fr.po"])
    r.index.add(["wiki/src/new.mdwn", "wiki/src/new.de.po", "wiki/src/new.fr.po"])
    r.index.commit("rename")
    r.create_tag('remote-wiki-rename')

    master.commit = r.commit("remote-1")
    master.checkout(force=True)
    r.index.remove(["a.de.po", "a.fr.po", "a.mdwn"])
    r.index.commit("delete")
    r.create_tag('remote-delete')

    master.commit = r.commit("remote-1")
    master.checkout(force=True)
    os.rename(os.path.join(repopath, "a.de.po"), os.path.join(repopath, "b.de.po"))
    os.rename(os.path.join(repopath, "a.fr.po"), os.path.join(repopath, "b.fr.po"))
    os.rename(os.path.join(repopath, "a.mdwn"), os.path.join(repopath, "b.mdwn"))
    r.index.remove(["a.de.po", "a.fr.po", "a.mdwn"])
    r.index.add(["b.de.po", "b.fr.po", "b.mdwn"])
    r.index.commit("rename")
    r.create_tag('remote-rename')

    master.commit = r.commit("master-localchanges")
    remote = r.commit('remote-1')
    master.checkout(force=True)

    base = r.merge_base(master, remote)
    index = git.IndexFile.from_tree(r, base, master, remote)
    shutil.copy(str(datapath/"a.it.po.masterAdded"), os.path.join(repopath, "a.it.po"))
    index.add(['a.it.po'])
    index.write()
    index.commit("a.it.po: adding as a.de.po has added.", parent_commits=(master.commit, remote))
    r.create_tag('master-added')

    master.commit = r.commit("remote-1")
    remoteHead.commit = r.commit("remote-1")
    master.checkout(force=True)
