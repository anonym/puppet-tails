#!/bin/bash
#
# a script to update the staging website
# run as user weblate!

set -e
set -u

STAGING_WEB_DIR=/var/www/staging
STAGING_REPO_DIR=/var/lib/weblate/repositories/vcs/staging
SANITY_CHECK_LOGFILE_BASENAME=last-sanity-errors.txt
SANITY_CHECK_LOGFILE="${STAGING_WEB_DIR}/${SANITY_CHECK_LOGFILE_BASENAME}"
IKIWIKI_LOGFILE=/var/log/weblate/update-staging-website_ikiwiki.log

_log() {
	msg=${1}
	echo "$( date -Is ) - ${*}" >> ${IKIWIKI_LOGFILE}
}

truncate -s 0 ${IKIWIKI_LOGFILE}

_log "Saving suggestions..."

/var/lib/weblate/scripts/save-suggestions.py "$STAGING_REPO_DIR" >> ${IKIWIKI_LOGFILE}

cd "$STAGING_REPO_DIR"

# Check generated .po files before running Ikiwiki build

_log "Checking .po files..."

while read f; do
        msgfmt -c -o /dev/null ${f} 2>&1 | grep -v " warning: " | tee -a ${IKIWIKI_LOGFILE}
        test ${PIPESTATUS[0]} = 0
done < <( find ./wiki/src -name "*.po" )

# Note: cron ignores the exit code so on errors, we print a message to
# STDERR, which cron will then send over email

_log "Running sanity checks..."

./bin/sanity-check-website > "$SANITY_CHECK_LOGFILE" 2>&1 \
    || echo "sanity-check-website failed; for details, see https://staging.tails.boum.org/${SANITY_CHECK_LOGFILE_BASENAME}" >&2

_log "Building the website with ikiwiki..."

ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --refresh >> "$IKIWIKI_LOGFILE" 2>&1 \
    || ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --rebuild >> "$IKIWIKI_LOGFILE" 2>&1 \
    || echo "ikiwiki failed; for details, see ${IKIWIKI_LOGFILE} on $(hostname -f)" >&2

_log "Finished!"
