import logging
import pathlib
import os
import sys

sys.path.insert(0, "/usr/local/share/weblate")
os.environ["DJANGO_SETTINGS_MODULE"] = "weblate.settings"
os.environ["DJANGO_IS_MANAGEMENT_COMMAND"] = "1"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from weblate.trans import models
from django.db.models import Q
from django.utils.text import slugify

logger = logging.getLogger(__name__)

# weblate project name
PROJECT = models.Project.objects.first()

# repo path ( should be start with weblate:// )
REPO = models.Component.objects.first().get_repo_link_url()

if not REPO.startswith("weblate://"):
    logger.error("We need a weblate:// url for searching for new components and not '{}'.".format(REPO))
    sys.exit(-1)

# We need to limit slug length to avoid problems with MySQL
# silent truncation
# pylint: disable=protected-access
slug_len = models.Component._meta.get_field('slug').max_length
name_len = models.Component._meta.get_field('name').max_length

def lock_repository(f):
    def func():
        repository = models.Component.objects.first().repository
        with repository.lock:
            f()
    return func

def filemask(path):
    if type(path) == str:
        path = pathlib.Path(path)

    parts = path.name.split(".")
    if parts[-1] == "po":
        base = path.with_name(".".join(parts[:-2]))
    elif len(parts) > 1:
        base = path.with_name(".".join(parts[:-1]))
    else:
        base = path

    return str(base) + ".*.po"


class TailsWeblateException(Exception):
    pass


def addComponent(path):
    """Adds a Weblate component that handles that path.
    Raises TailsWeblateException if there is alaready a subProject that handles the file."""
    _filemask = filemask(path)
    name = _filemask[:name_len]
    slug = slugify(name)[:slug_len]

    components = models.Component.objects.filter(
        Q(name=name) | Q(slug=slug),
        project=PROJECT
    )

    if components.exists():
        raise TailsWeblateException("%s already exists" % name)

    return models.Component.objects.create(
        name=name,
        slug=slug,
        filemask=_filemask,
        project=PROJECT,
        repo=REPO,
        branch='master',
        vcs="git",
        template='',
        license='GPL v3',
        file_format="po",
        committer_name="Tails translators",
        committer_email="tails-l10n@boum.org",
        enable_suggestions=True,
        suggestion_voting=True,
        suggestion_autoaccept=0,
        allow_translation_propagation=True,
        push_on_commit=False,
    )


def renameComponent(oldPath, newPath):
    """Renames a component according to the reference files (oldPath -> newPath).
    Raises TailsWeblateException if new component already exists.
    """
    try:
        subProject(newPath)
        raise TailsWeblateException("%s already exists" % newPath)
    except ComponentExceptionNotFound:
        pass

    component = subProject(oldPath)
    _filemask = filemask(newPath)
    component.filemask = _filemask
    component.slug = slugify(component.name)[:slug_len]
    component.save()  # Triggers create_translations and update the translations.


class ComponentException(Exception):
    def __init__(self, errno, msg, args):
        self.errno = errno
        self.msg = msg
        self.args = args

    def __str__(self):
        return "{}({}): {}".format(self.__class__.__name__, self.errno, self.msg)


class ComponentExceptionNotFound(ComponentException):
    ERRNO = 1

    def __init__(self, filemask):
        msg = "component not found for {}".format(filemask)
        ComponentException.__init__(self, self.ERRNO, msg, {"filemask": filemask})


class ComponentExceptionWeblatUrl(ComponentException):
    ERRNO = 2

    def __init__(self, filemask, component):
        msg = "component for {} has no weblate url {}".format(filemask, component.repo)
        ComponentException.__init__(self, self.ERRNO, msg, {"filemask": filemask, "component": component})


def subProject(fpath):
    """returns the corresponding Weblate component, that handles this file.
    Raises a ComponentExceptionNotFound, if there is no component found."""

    _filemask = filemask(fpath)

    component = models.Component.objects.filter(filemask=_filemask).first()
    if component is None:
        raise ComponentExceptionNotFound(_filemask)

    return component
