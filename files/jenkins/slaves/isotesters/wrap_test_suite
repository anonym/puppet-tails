#!/bin/sh

# This script is called from by test_Tails_ISO_* jobs,
# defined in jenkins-jobs.git.

set -e
set -u
set -x

### Process command line and environment

ISO_HISTORY_URL="$1"
shift
TMP_DIR="$1"
shift
: "${USE_LAST_RELEASE_AS_OLD_ISO:=yes}"

### Constants

ALREADY_RUN_FLAG_FILE="${TMP_DIR}/already_run"
FIRST_BUSTER_SPECIFIC_COMMIT=2f106487f6a118e76cf0d7d47ba260e087a64ab2

### Sanity checks

[ "$WORKSPACE" ] || exit 2
[ "$ISO_HISTORY_URL" ] || exit 3
[ "$TMP_DIR" ] || exit 4
[ "$UPSTREAMJOB_GIT_COMMIT" ] || exit 5
# shellcheck disable=SC2153
[ "$UPSTREAMJOB_GIT_BASE_BRANCH_HEAD" ] || exit 6
[ "$UPSTREAMJOB_BUILD_NUMBER" ] || exit 7
[ "$JENKINS_URL" ] || exit 8
[ "$JOB_NAME" ] || exit 9
[ "$BUILD_NUMBER" ] || exit 10
[ "$USE_LAST_RELEASE_AS_OLD_ISO" = yes ] \
    || [ "$USE_LAST_RELEASE_AS_OLD_ISO" = no ] \
    || exit 13

[ -d "$TMP_DIR" ] || exit 11

if [ -f "$ALREADY_RUN_FLAG_FILE" ]; then
    echo "A test suite job has already been run on this ISO tester." >&2
    echo "The reboot job has probably failed." >&2
    echo "Please report this to the Tails system administrators." >&2
    exit 12
fi

### Functions

as_root_do() {
    sudo -n \
    ${JENKINS_URL:+JENKINS_URL="$JENKINS_URL"} \
    ${WORKSPACE:+WORKSPACE="$WORKSPACE"} \
    ${TMP_DIR:+TMP_DIR="$TMP_DIR"} \
    ${UPSTREAMJOB_ISO:+UPSTREAMJOB_ISO="$UPSTREAMJOB_ISO"} \
    ${PREVIOUS_ISO:+PREVIOUS_ISO="$PREVIOUS_ISO"} \
    "${@}"
}

only_changes_doc() {
    local upstreamjob_git_base_branch_head="$1"

    local base_branch_non_doc_diff="$(git diff \
        "${upstreamjob_git_base_branch_head}..." \
        -- \
        '*' \
        ':!/wiki' \
        ':!/ikiwiki.setup' \
        ':!/ikiwiki-cgi.setup' \
        ':!*.po')"

    [ -z "${base_branch_non_doc_diff}" ]
}

based_on_buster() {
    git merge-base --is-ancestor \
        "$FIRST_BUSTER_SPECIFIC_COMMIT" \
        "$(git rev-parse --verify HEAD)"
}

tails_4_0_was_released() {
    git merge-base --is-ancestor \
        "$FIRST_BUSTER_SPECIFIC_COMMIT" \
        "$(git rev-parse --verify origin/master)"
}

use_last_release_as_old_iso() {
    [ "$USE_LAST_RELEASE_AS_OLD_ISO" = yes ] \
       && ( tails_4_0_was_released || ! based_on_buster )
}

### Main

as_root_do touch "$ALREADY_RUN_FLAG_FILE"

git reset --hard "${UPSTREAMJOB_GIT_COMMIT}"
git merge --no-edit "${UPSTREAMJOB_GIT_BASE_BRANCH_HEAD}"

UPSTREAMJOB_ISO=$(ls tmp/tails-*.iso)

if use_last_release_as_old_iso; then
    LAST_RELEASE_VERSION=$(git show origin/master:debian/changelog | dpkg-parsechangelog -l- -S version)
    PREVIOUS_ISO="tails-amd64-${LAST_RELEASE_VERSION}.iso"
    PREVIOUS_IMG="tails-amd64-${LAST_RELEASE_VERSION}.img"
    wget --no-verbose \
         "${ISO_HISTORY_URL}/tails-amd64-${LAST_RELEASE_VERSION}/${PREVIOUS_ISO}"
    wget --no-verbose \
         "${ISO_HISTORY_URL}/tails-amd64-${LAST_RELEASE_VERSION}/${PREVIOUS_IMG}"
else
    PREVIOUS_ISO="${UPSTREAMJOB_ISO}"
fi

cp -a /etc/TailsToaster/* "${WORKSPACE}/features/config/"

# Use the local email server (#12277)
perl -pi -E \
     "s{\A(\s*address:\s*).*}{\$1test\@$(hostname --fqdn)} ; \
      s{\A(\s*password:\s*).*}{\$1test}" \
     "${WORKSPACE}/features/config/common.d/thunderbird.yml"

if echo "${UPSTREAMJOB_GIT_BRANCH}" | grep -q -E '[+-]force-all-tests$' \
   || [ "${GIT_BRANCH#origin/}" = testing ] \
   || [ "${GIT_BRANCH#origin/}" = feature/tor-nightly-master ] \
   || [ "${GIT_BRANCH#origin/}" = devel ] ; then
    TAGS_ARGS=""
elif git describe --tags --exact-match "${UPSTREAMJOB_GIT_COMMIT}" >/dev/null 2>&1; then
    TAGS_ARGS=""
elif [ "${UPSTREAMJOB_GIT_COMMIT}" != "${UPSTREAMJOB_GIT_BASE_BRANCH_HEAD}" ] && \
       only_changes_doc "$UPSTREAMJOB_GIT_BASE_BRANCH_HEAD"; then
    TAGS_ARGS="--tag ~@fragile --tag @doc"
else
    TAGS_ARGS="--tag ~@fragile"
fi

as_root_do ./run_test_suite \
    --old-iso "${PREVIOUS_ISO}" \
    --iso "${UPSTREAMJOB_ISO}" \
    --artifacts-base-uri "${JENKINS_URL}job/${JOB_NAME}/${BUILD_NUMBER}/artifact/build-artifacts/" \
    --capture \
    -- \
    $TAGS_ARGS \
    "${@}"
