#!/usr/bin/python3
import unittest
import subprocess

class MeetingTest(unittest.TestCase):
    faketime = "faketime"
    command = "./meeting.py"
    date = "2018-12-23"

    def test1(self):
        date = "2008-12-24"
        output = subprocess.check_output([self.faketime, date, self.command])
        self.assertEqual(output, b'The monthly Tails meeting in January will be on Tuesday 06. January 2009\n')

    def test2(self):
        output = subprocess.check_output([self.faketime, self.date, self.command])
        self.assertEqual(output, b'The monthly Tails meeting in January will be on Thursday 03. January 2019\n')

    def test3(self):
        output = subprocess.check_output([self.faketime, self.date, self.command, '-m', '3'])
        self.assertEqual(output, b'The monthly Tails meeting in March will be on Wednesday 06. March 2019\n')

    def test4(self):
        output = subprocess.run([self.faketime, self.date, self.command, '-d', '32'], check=False, capture_output=True)
        self.assertEqual(output.stderr, b'The month/day combination month: 12, day: 32 is invalid: day is out of range for month\n')

    def test5(self):
        output = subprocess.check_output([self.faketime, self.date, self.command, '-m', '1', '-d', '27'])
        self.assertEqual(output, b'The monthly Tails meeting in January will be on Wednesday 30. January 2019\n')

    def test6(self):
        date = "2008-11-24"
        output = subprocess.check_output([self.faketime, date, self.command])
        self.assertEqual(output, b'The monthly Tails meeting in December will be on Wednesday 03. December 2008\n')

    def test7(self):
        date = "2008-01-24"
        output = subprocess.check_output([self.faketime, date, self.command])
        self.assertEqual(output, b'The monthly Tails meeting in February will be on Wednesday 06. February 2008\n')

if __name__ == '__main__':
    unittest.main()
